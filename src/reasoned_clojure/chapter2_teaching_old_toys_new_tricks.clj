(ns reasoned-clojure.chapter2-teaching-old-toys-new-tricks
  (:refer-clojure :exclude [==])
  (:require [clojure.core.logic :refer :all]))

; == and ≡ are interchangeable
(def ≡ ==)

;2.1
;What is the value of
(first '(grape raisin pear))

;2.2
;What is the value of
(first '(a c o r n))

;2.3
;What value is associated with q in
(run* [q]
      (firsto '(a c o r n)
              q))

;because a is the first of (a c o r n).

;2.4
;What value is associated with q in

(run* [q]
      (firsto '(a c o r n)
              'a))

; because a is the first of (a c o r n), thus the goal succeeds.

;2.5
;What value is associated with r in

(run* [r]
      (fresh [x y]
             (firsto `(~r ~y) x)
             (≡ 'pear x)))

; Since the first of `(~r ~y), which is the fresh variable r,
; is fused with x. Then pear is associated with x, which
; in turn associates pear with r.

;2.6
; Here is firsto, defined as firstoo.
(defn firstoo
  [lst fst-lst]
  (fresh [rest]
         (≡ (lcons fst-lst rest)
            lst)))
; What is unusual about this definition?
;
; Whereas first expects one argument, firsto expects two.
; I will show that they are equivalent:

(run* [r]
      (fresh [x y]
             (firstoo `(~r ~y) x)
             (≡ 'pear x)))


; 2.7
; What is the value of
(cons (first '(grape raisin pear))
      (first '((a) (b) (c))))
;That’s familiar...

; 2.8
;What value is associated with r in
(run* [r]
      (fresh [x y]
             (firsto '(grape raisin pear) x)
             (firsto '((a) (b) (c)) y)
             (≡ (lcons x y) r)))

; 2.9
;What does lcons stand for?

; logical cons, it is used in place of cons when working with
; logic programming

; 2.10
;What is the value of
(rest '(grape raisin pear))

; 2.11
;What is the value of
(first (rest (rest '(a c o r n))))

;2.12
;What value is associated with r in
(run* [r]
      (fresh [v]
             (resto '(a c o r n) v)
             (fresh [w]
                    (resto v w)
                    (firsto w r))))

; The process of transforming
; (first (first (rest l))) into (resto l v), (resto v w),
; and (firsto w r) is called unnesting. We introduce
; fresh expressions as necessary as we unnest.

; this could also be written like so:
(run* [r]
      (fresh [v w]
             (resto '(a c o r n) v)
             (resto v w)
             (firsto w r)))

;2.13 interesting
;define resto, define it as restoo, run the form below and
;make sure the results are the same as in 2.12

(defn restoo
  [lst r-lst]
  (fresh [a]
         (== (lcons a r-lst)
             lst)))

(run* [r]
      (fresh [v w]
             (restoo '(a c o r n) v)
             (restoo v w)
             (firsto w r)))

;2.14
;What is the value of
(cons (rest '(grape raisin pear))
      (first '((a) (b) (c))))

;2.15
; What value is associated with r in:
(run* [r]
      (fresh [x y]
             (resto '(grape raisin pear) x)
             (firsto '((a) (b) (c)) y)
             (≡ (lcons x y) r)))

;2.16
;What value is associated with q in
(run* [q]
      (resto '(a c o r n) '(c o r n)))
;because (c o r n) is the rest of (a c o r n).

;2.17
;What value is associated with x in
(run* [x]
      (resto '(c o r n) `(~x ~'r ~'n)))

;2.18
;What value is associated with l in
(run* [l]
      (fresh [x]
             (resto l '(c o r n))
             (firsto l x)
             (≡ 'a x)))

; because if the rest of l is (c o r n), then
; the list `(~a c o r n) is associated with l,
; where a is the variable introduced in the
; definition of resto. The firsto of l, a,
; fuses with x. When we associate a with x,
; we also associate a with 'a, so the list
; (a c o r n) is associated with l.

;2.19
;What value is associated with l in
(run* [l]
      (conso '(a b c) '(d e) l))
; since conso associates the value of
; (cons '(a b c) '(d e)) with l.

;2.20
;What value is associated with x in
(run* [x]
      (conso x '(a b c) '(d a b c)))

; Since (cons 'd '(a b c)) is (d a b c),
; conso associates d with x.

;2.21
;What value is associated with r in
(run* [r]
      (fresh [x y z]
             (≡ `(~'e ~'a ~'d ~x) r)
             (conso y `(~'a ~z ~'c) r)))

;2.22

;What value is associated with x in
(run* [x]
      (conso x
             (llist 'a x 'c)
             (llist 'd 'a x 'c)))

; the value we can associate with x so that
;(cons x `(a ~x c)) is `(d a ~x c).

;2.23

;What value is associated with l in
(run* [l]
      (fresh [x]
             (≡ (llist 'd 'a x 'c) l)
             (conso x (llist 'a x 'c) l)))


;2.24
;What value is associated with l in:

(run* [l]
      (fresh [x]
             (conso x (llist 'a x 'c) l)
             (≡ (llist 'd 'a x 'c) l)))

;(d a d c) ignore the . for now

;2.25 interesting
;define conso using firsto and resto

(defn conso2
  [first-lst lst result]
  (and* [(firsto result first-lst)
         (resto result lst)]))


;2.26 interesting
; Now, define the conso relation using ≡ instead
; of firsto and resto.

(defn conso3
  [first rest result]
  (≡ result (llist first rest)))


;2.27
;Here’s a bonus question. What value is associated with l in:

;;TODO review this again
(run* [l]
      (fresh [d t x y w]
             (conso w '(n u s) t)
             (resto l t)
             (firsto l x)
             (≡ 'b x)
             (resto l d)
             (firsto d y)
             (≡ 'o y)))

;It’s a five-element list.

;2.28
; what is the value of:

(nil? '(grape raisin pear))

;2.29
;What is the value of:
(nil? '())


;2.30
; What is the value of:
(run* [q] (nilo '(grape raisin pear)))

;2.31
;What is the value of
(run* [q] (nilo '()))


;2.32

;What is the value of
(run* [x] (nilo x))

;since the only way (nilo x) succeeds is if nil is associated with x.


;2.33
;Define nilo2 using ≡.

(defn nilo2
  [null?]
  (== nil null?))

;2.34
;Is (split . pea) a pair?
; Yes

;2.35
;Is `(split . ~x) a pair?
; Yes

; assume that the next few lines that contain pair? is a function that returns a boolean
; the function determines whether the argument that was passed to it is a pair
;2.36
; What is the value of:
;(pair? '((split) . pea))
; true

;2.37
;What is the value of
; (pair? '())
; false

;2.38
; Is pair a pair?
; No

;2.39
; Is pear a pair?
; no

;2.40
;Is (pear) a pair?
; yes, it is the pair (pear . ()).

;2.41
;What is the value of
(first '(pear))

;2.42
;What is the value of
(rest '(pear))

;2.43
; How do we build these pairs?
; Use lcons the magnificent

;2.44
;What is the value of
(lcons '(split) 'pea)

;2.45
; What value is associated with r in:
(run* [r]
     (fresh [x y]
            (== (lcons x (lcons y 'salad))
                r)))

;2.46
; here is a pair
(defn pairo
  [p]
  (fresh [a d]
         (conso a d p)))

; is pairo recursive?

; no it is not

;2.47
;What is the value of
(run* [q]
      (pairo (lcons q '(q))))

;(cons q q) creates a pair of the same fresh variable. But we are not interested in the pair, only q.

;2.48
;What is the value of
(run* [q] (pairo '()))

;2.49
;What is the value of
(run* [q] (pairo 'pair))

;2.50
; What value is associated with x in
(run* [x] (pairo x))

;2.51
; What value is associated with r in
(run* [r]
    (pairo (lcons r '())))

;2.52
; Is (tofu) a singleton?
; Yes, because it is a list of a single value, tofu.

;2.53
;Is ((tofu)) a singleton?
;Yes, because it is a list of a single value, (tofu).

;2.54
;Is tofu a singleton?
; No, because it is not a list of a single value.

;2.55
; Is (e tofu) a singleton?
; No, because it is not a list of a single value.

;2.56
; Is () a singleton?
; No, because it is not a list of a single value.

;2.57
; Is (e . tofu) a singleton?
; No, because (e . tofu) is not a list of a single value.













;; TODO adventure in prolog, translate it to core.logic

