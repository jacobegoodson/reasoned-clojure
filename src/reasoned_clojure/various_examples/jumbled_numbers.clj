(ns reasoned-clojure.various-examples.jumbled-numbers)

(def nums-map {"one" "1" "two" "2" "three" "3" "four" "4" "five" "5" "six" "6" "seven" "7" "eight" "8" "nine" "9" "zero" "0"})

(defn extract-num
  [m num]
  (reduce (fn [acc c]
            (if-let [v (acc c)]
                    (if (= 1 v)
                        (dissoc acc c)
                        (update acc c dec))
                    (reduced nil)))
          m
          num))

(defn inner-solve-it
  [word nums out]
  (cond (empty? word) out
        (empty? nums) nil
        :else         (if-let [result (if-let [result (extract-num word (first nums))]
                                              (inner-solve-it result nums (str out (nums-map (first nums))))
                                              (inner-solve-it word (rest nums) out))]
                              result
                              (inner-solve-it word (rest nums) out))))

(defn solve-it
  [word]
  (inner-solve-it (frequencies word)
                  ["zero" "one" "two" "three" "four" "five" "six" "seven" "eight" "nine"]
                  ""))

(solve-it "seveneightninetwothreefive")