(ns reasoned-clojure.specter-problems.problems
  (:require [com.rpl.specter :refer :all]))

;; replace first blank to see if it passes the tests
(defmacro pass-all
  [solution & forms]
  `(every? true?
           ~(transform [(walker (fn [x] (= x '__)))]
                       (fn [_] solution)
                       (into [] forms))))

;1
(pass-all __ (= __ true))

;2
(pass-all __ (= (- 10 (* 2 3)) __))

;3
(= __ (.toUpperCase "hello world"))

;4
(= (list __) '(:a :b :c))

;5
(pass-all __
          (= __ (conj '(2 3 4) 1))
          (= __ (conj '(3 4) 2 1)))

;6 Vectors can be constructed several ways. You can compare them with lists.
(= [__] (list :a :b :c) (vec '(:a :b :c)) (vector :a :b :c))

;Problem 7
;When operating on a Vector, the conj function will return a new vector with one or more items "added" to the end.

(pass-all __
          (= __ (conj [1 2 3] 4))
          (= __ (conj [1 2] 3 4)))

;Problem 8
;Sets are collections of unique values.

(pass-all __
          (= __ (set '(:a :a :b :c :c :c :c :d :d)))
          (= __ (clojure.set/union #{:a :b :c} #{:b :c :d})))

;Problem 9
; When operating on a set, the conj function returns a
; new set with one or more keys "added".
(pass-all __
          (= #{1 2 3 4} (conj #{1 4 3} __)))

;Problem 10
; Maps store key-value pairs. Both maps and keywords can be used as
; lookup functions. Commas are whitespace.

(pass-all __
          (= __ ((hash-map :a 10, :b 20, :c 30) :b))
          (= __ (:b {:a 10, :b 20, :c 30})))

;Problem 11
; When operating on a map, the conj function returns a new map with one
; or more key-value pairs "added".

(pass-all __
          (= {:a 1, :b 2, :c 3} (conj {:a 1} __ [:c 3])))

;Problem 12
; All Clojure collections support sequencing. You can operate on sequences
; with functions like first, second, and last.

(pass-all __
          (= __ (first '(3 2 1)))
          (= __ (second [2 3 4]))
          (= __ (last (list 1 2 3))))

;Problem 13
;The rest function will return all the items of a sequence except the first.

(pass-all __
          (= __ (rest [10 20 30 40])))


;Problem 14
;Clojure has many different ways to create functions.

(pass-all __
          (= __ ((fn add-five [x] (+ x 5)) 3))
          (= __ ((fn [x] (+ x 5)) 3))
          (= __ (#(+ % 5) 3))
          (= __ ((partial + 5) 3)))


;Problem 15
;Write a function which doubles a number.

(pass-all __
          (= (__ 2) 4)
          (= (__ 3) 6)
          (= (__ 11) 22)
          (= (__ 7) 14))

;Problem 16
;Write a function which returns a personalized greeting.

(pass-all __
          (= (__ "Dave") "Hello, Dave!")
          (= (__ "Jenn") "Hello, Jenn!")
          (= (__ "Rhea") "Hello, Rhea!"))

;Problem 17
;The map function takes two arguments: a function (f) and
; a sequence (s). Map returns a new sequence consisting of the
; result of applying f to each item of s. Do not confuse the
; map function with the map data structure.

(pass-all __
          (= __ (map #(+ % 5) '(1 2 3))))

;Problem 18
;The filter function takes two arguments: a predicate function
; (f) and a sequence (s). Filter returns a new sequence consisting
; of all the items of s for which (f item) returns true.

(pass-all __
          (= __ (filter #(> % 5) '(3 4 5 6 7))))

;Problem 19
;Write a function which returns the last element in a sequence.
; Special Restrictions : last

(pass-all __
          (= (__ [1 2 3 4 5]) 5)
          (= (__ '(5 4 3)) 3)
          (= (__ ["b" "c" "d"]) "d"))

;Problem 20
;Write a function which returns the second to last element from a sequence.

(pass-all __
          (= (__ (list 1 2 3 4 5)) 4)
          (= (__ ["a" "b" "c"]) "b")
          (= (__ [[1 2] [3 4]]) [1 2]))


;Problem 21
;Write a function which returns the Nth element from a sequence.
;Special Restrictions : nth

(pass-all __
          (= (__ '(4 5 6 7) 2) 6)
          (= (__ [:a :b :c] 0) :a)
          (= (__ [1 2 3 4] 1) 2)
          (= (__ '([1 2] [3 4] [5 6]) 2) [5 6]))


;Problem 22
;Write a function which returns the total number of elements in a sequence.

(pass-all __
          (= (__ '(1 2 3 3 1)) 5)
          (= (__ "Hello World") 11)
          (= (__ [[1 2] [3 4] [5 6]]) 3)
          (= (__ '(13)) 1)
          (= (__ '(:a :b :c)) 3))

;Problem 23
;Write a function which reverses a sequence.

(pass-all __
          (= (__ [1 2 3 4 5]) [5 4 3 2 1])
          (= (__ (sorted-set 5 7 2 7)) '(7 5 2))
          (= (__ [[1 2][3 4][5 6]]) [[5 6][3 4][1 2]]))

;Problem 24
;Write a function which returns the sum of a sequence of numbers.

(pass-all __
          (= (__ [1 2 3]) 6)
          (= (__ (list 0 -2 5 5)) 8)
          (= (__ #{4 2 1}) 7)
          (= (__ '(0 0 -1)) -1)
          (= (__ '(1 10 3)) 14))

;Problem 25
;Write a function which returns only the odd numbers from a sequence.

(pass-all __
          (= (__ #{1 2 3 4 5}) '(1 3 5))
          (= (__ [4 2 1 6]) '(1))
          (= (__ [2 2 4 6]) '())
          (= (__ [1 1 1 3]) '(1 1 1 3)))

;Problem 26
;Write a function which returns the first X fibonacci numbers.

(pass-all __
          (= (__ 3) '(1 1 2))
          (= (__ 6) '(1 1 2 3 5 8))
          (= (__ 8) '(1 1 2 3 5 8 13 21)))

;Problem 27
;Write a function which returns true if the given sequence
; is a palindrome. Hint: "racecar" does not equal '(\r \a \c \e \c \a \r)

(pass-all __
          (false? (__ '(1 2 3 4 5)))
          (true? (__ "racecar"))
          (true? (__ [:foo :bar :foo]))
          (true? (__ '(1 1 3 3 1 1)))
          (false? (__ '(:a :b :c))))

;Problem 28
;Write a function which flattens a sequence.

(def LEAVES
  (recursive-path [] p
                  (if-path (fn [x] (and (seqable? x) (not (string? x))))
                           [ALL p]
                           STAY)))

(pass-all #(select LEAVES %)
          (= (__ '((1 2) 3 [4 [5 6]])) '(1 2 3 4 5 6))
          (= (__ ["a" ["b"] "c"]) '("a" "b" "c"))
          (= (__ '((((:a))))) '(:a)))

;Problem 29
; Write a function which takes a string and returns a new string
; containing only the capital letters.

(pass-all (fn [x] (apply str (select [ALL #(Character/isUpperCase %)] x)))
          (= (__ "HeLlO, WoRlD!") "HLOWRD")
          (empty? (__ "nothing"))
          (= (__ "$#A(*&987Zf") "AZ"))

;Problem 30
;Write a function which removes consecutive duplicates from a sequence.

(pass-all __
          (= (apply str (__ "Leeeeeerrroyyy")) "Leroy")
          (= (__ [1 1 2 3 3 2 2 3]) '(1 2 3 2 3))
          (= (__ [[1 2] [1 2] [3 4] [1 2]]) '([1 2] [3 4] [1 2])))

;Problem 31
;Write a function which packs consecutive duplicates into sub-lists.

(pass-all __
          (= (__ [1 1 2 1 1 1 3 3]) '((1 1) (2) (1 1 1) (3 3)))
          (= (__ [:a :a :b :b :c]) '((:a :a) (:b :b) (:c)))
          (= (__ [[1 2] [1 2] [3 4]]) '(([1 2] [1 2]) ([3 4]))))

;Problem 32
;Write a function which duplicates each element of a sequence.

(pass-all __
          (= (__ [1 2 3]) '(1 1 2 2 3 3))
          (= (__ [:a :a :b :b]) '(:a :a :a :a :b :b :b :b))
          (= (__ [[1 2] [3 4]]) '([1 2] [1 2] [3 4] [3 4]))
          (= (__ [44 33]) [44 44 33 33]))

;Problem 33
;Write a function which replicates each element of a sequence a variable number of times.

(pass-all __
          (= (__ [1 2 3] 2) '(1 1 2 2 3 3))
          (= (__ [:a :b] 4) '(:a :a :a :a :b :b :b :b))
          (= (__ [4 5 6] 1) '(4 5 6))
          (= (__ [[1 2] [3 4]] 2) '([1 2] [1 2] [3 4] [3 4]))
          (= (__ [44 33] 2) [44 44 33 33]))

;Problem 34
;Write a function which creates a list of all integers in a given range.
;Special Restrictions : range

(pass-all __
          (= (__ 1 4) '(1 2 3))
          (= (__ -2 2) '(-2 -1 0 1))
          (= (__ 5 8) '(5 6 7)))

;Problem 35
;Clojure lets you give local names to values using the special let-form.
(pass-all __
          (= __ (let [x 5] (+ 2 x)))
          (= __ (let [x 3, y 10] (- y x)))
          (= __ (let [x 21] (let [y 3] (/ x y)))))

;Problem 36
;Can you bind x, y, and z so that these are all true?

(pass-all __
          (= 10 (let __ (+ x y)))
          (= 4 (let __ (+ y z)))
          (= 1 (let __ z)))

;Problem 37
;Regex patterns are supported with a special reader macro.
(pass-all __
          (= __ (apply str (re-seq #"[A-Z]+" "bA1B3Ce "))))

;Problem 38