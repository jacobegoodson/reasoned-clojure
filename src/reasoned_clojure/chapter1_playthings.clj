(ns reasoned-clojure.chapter1-playthings
  (:refer-clojure :exclude [==])
  (:require [clojure.core.logic :refer :all]
            [clojure.core.logic.pldb ::refer :all]))

; started later in the chapter because that is
; when I realised that I wanted to track
; the translation

;; All examples are translated from:
;; Friedman, Daniel P.. The Reasoned Schemer, second edition. MIT Press. Kindle Edition.

; == and ≡ are interchangeable
(def ≡ ==)

;; 1.56

; What is the value of
(run* [q]
      (conde [(== 'olive q)]
             [fail]))
;; or is shorthand notation for conde, where each goal does not have to be placed in
;; square brackets
(run* [q]
      (or* [(== 'olive q) fail]))

; because the goal (conde [g1] [g2]), (or* [g1 g2])
; succeeds if either g1 or g2 succeeds.

;; 1.58

;What is the value of

(run* [q]
      (conde [fail]
             [(≡ 'oil q)]))

;because the goal (conde g1 g2) succeeds if either g1 or g2 succeeds.

;; 1.59

;What is the value of

(run* [q]
      (conde [(≡ 'olive q)]
             [(≡ 'oil q)]))

; a list of two values. Both goals contribute values.
; (≡ 'olive q) succeeds, and olive is the first value
; associated with q. (≡ 'oil q) also succeeds, and oil
; is the second value associated with q.

;; 1.60

; What is the value of

(run* [q]
      (fresh [x]
             (fresh [y]
                    (conde [(≡ `(~x ~y) q)]
                           [(≡ `(~y ~x) q)]))))

; because conde contributes two values. In the first value,
; x is reified as _0 and y is reified as _1. In the second
; value, y is reified as _0 and x is reified as _1.

;; 1.61

; Correct! The variables x and y are not fused in the previous
; run* expression, however. Each value produced by a run*
; expression is reified independently of any other values.
; This means that the numbering of reified variables begins
; again, from 0, within each reified value.
; Okay.

;; 1.62

;;What is the value of

(run* [x]
      (conde [(≡ 'olive x) fail]
             [(≡ 'oil x)]))


;; 1.63

;What is the value of

(run* [x]
      (conde  [(≡ 'olive x) succeed]
              [(≡ 'oil x)]))

;; 1.64
;What is the value of
(run* [x]
      (conde [(≡ 'oil x)]
             [(≡ 'olive x) succeed]))

;; 1.65

;What is the value of

(run* [x]
      (conde [(≡ 'virgin x) fail]
             [(conde [(≡ 'olive x)]
                     [(conde [succeed]
                             [(≡ 'oil x)])])]))
; alternate form
(run* [x]
      (or* [(and* [(≡ 'virgin x) fail])
            (or* [(≡ 'olive x)
                  (or* [succeed
                        (≡ 'oil x)])])]))


; The goal [(≡ 'virgin x) fail] fails. Therefore, the body
; of the run* behaves the same as the second conde,
; [(conde [(≡ 'olive x)]
;         [(conde [succeed]
;                 [(≡ 'oil x)])])]


;;1.66

; QUESTION
;
; In the previous question’s expression, whose value is (olive _0 oil),
; how do we end up with _0
;
; ANSWER
;
; Through the succeed in the innermost
; disj2, which succeeds without associating a value with x.

;;1.67
;What is the value of this run* expression?

(run* [r]
      (fresh [x]
             (fresh [y]
                    (≡ 'split x)
                    (≡ 'pea y)
                    (≡ `(~x ~y) r))))


;;1.68
;Is the value of this run* expression
(run* [r]
      (fresh [x]
             (fresh [y]
                    (and* [(and* [(≡ 'split x) (≡ 'pea y)])
                           (≡ `(~x ~y) r)]))))
;the same as that of the previous frame?

;Yes. Can we make this run* expression shorter?

;;1.69
;Is this,
(run* [r]
      (fresh [x]
             (fresh [y]
                    (and* [(and* [(≡ 'split x) (≡ 'pea y)])
                           (≡ `(~x ~y) r)]))))

; shorter?
; Very funny. Is there another way to simplify this run* expression?

;1.70
; Yes. If fresh were able to create any number of variables,
; how might we rewrite the run* expression in the previous frame?

; ANSWER
; Like this

(run* [r]
      (fresh [x y]
             (≡ 'split x)
             (≡ 'pea y)
             (≡ `(~x ~y) r)))

; 1.71
; Does the simplified expression in the previous frame still produce
; the value ((split pea))

; Yes. Can we keep simplifying this expression?

;1.72
;Sure. If run* were able to create any number of fresh variables,
; how might we rewrite the expression from question 1.70?

; As this simpler expression,
(run* [r x y]
      (≡ 'split x)
      (≡ 'pea y)
      (≡ `(~x ~y) r))


;1.73

; Does the expression in the previous frame still produce the value
; ((split pea))
;
; No. The previous frame’s run* expression produces
; ([(split pea) split pea]), which is a vector containing
; the values associated with r, x, and y, respectively.


;1.74
; How can we change the expression in frame 72 to get back the value
; from frame 70, ((split pea))

; We can begin by removing r from the run* variable list.

;1.75

; Okay, so far. What else must we do, once we remove r from the
; run* variable list?


; We must remove (≡ `(~x ~y) r), clojure returns a vector instead
; of a list so we need one more transformation in order to produce
; the same return value Here is the new run* expression,
(map (partial apply list)
     (run* [x y]
           (≡ 'split x)
           (≡ 'pea y)))

; 1.76
; What is the value of
(run* [x y]
      (conde [(≡ 'split x)
              (≡ 'pea y)]
             [(≡ 'red x)
              (≡ 'bean y)]))


; 1.77
;Good guess! What is the value of

(run* [r]
      (fresh [x y]
             (conde
               [(≡ 'split x)
                (≡ 'pea y)]
               [(≡ 'red x)
                (≡ 'bean y)])
             (≡ `(~x ~y ~'soup) r)))


; 1.78
; Can fresh have more than two goals?

; YES

;1.79

;Rewrite the fresh expression from 1.77 in terms of or* and and*
; (fresh (x … ) (conde g1 g2) g3)) conde.

(run* [r]
      (fresh [x y]
             (or* [(and* [(≡ 'split x)
                          (≡ 'pea y)])
                   (and* [(≡ 'red x)
                          (≡ 'bean y)])])
             (≡ `(~x ~y ~'soup) r)))

;1.81
;How can we simplify this run* expression?
(run* [x y] (and* [(≡ 'split x) (≡ 'pea y)]))

;like this
(run* [x y] (≡ 'split x) (≡ 'pea y))

; 1.82
; Consider this very simple definition.

(defn teacup
  [t]
  (conde [(== 'tea t)]
         [(== 'cup t)]))

; this is considered a relation, what is a
; relation?

; 1.83

;A relation is a kind of function† that, when given arguments,
; produces a goal.
; What is the value of:
(run* [x]
      (teacup x))

;1.84

;What is the value of:

(run* [x y]
      (conde [(≡ false x) (≡ true y)]
             [(teacup x) (≡ true y)]))

; First (≡ false x) associates false with x, then (teacup x)
; associates tea with x, and finally (teacup x) associates cup with x.

;1.85

;What is the value of
(run* [x y]
      (teacup x)
      (teacup y))

;1.86

;What is the value of
(run* [x y]
      (teacup x)
      (teacup x))

; The first (teacup x) associates tea with x and then associates
; cup with x, while the second (teacup x) already has the
; correct associations for x, so it succeeds without associating
; anything. y remains fresh.

;Why are these results all the same?

(run* [x y]
      (teacup x))

(run* [x y]
      (teacup x)
      (teacup x)
      (teacup x)
      (teacup x)
      (teacup x))

; all the bindings of x must be the same within a unification


;1.87

; And what is the value of

(run* [x y]
      (conde [(teacup x) (teacup x)]
             [(≡ false x) (teacup y)]))

;1.90
; What is the value of

(run* [x y]
      (conde [(fresh [z]
                     (≡ 'lentil z))]
             [(≡ x y)]))

; In the first conde line x remains different from y, and both
; are fresh. lentil is associated with z, which is not reified.
; In the second conde line, both x and y remain fresh, but x is
; fused with y.

; 1.91
; What is the value of:
(run* [x y]
      (conde [(≡ 'split x) (≡ 'pea y)]
             [(≡ 'red x) (≡ 'bean y)]
             [(≡ 'green x) (≡ 'lentil y)]))

; 1.93
; What does the e in conde stand for?

; It stands for every, since every successful conde line
; contributes one or more values.


